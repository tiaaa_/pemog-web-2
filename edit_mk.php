<?php

include 'koneksi.php';

 if (isset($_GET['kode_mk'])) {

 $kode_mk = ($_GET["kode_mk"]);

 $query = "SELECT * FROM tb_mk WHERE kode_mk='$kode_mk'";
 $result = mysqli_query($koneksi, $query);
 
 if(!$result){
 die ("Query Error: ".mysqli_errno($koneksi).
 " - ".mysqli_error($koneksi));
 }
 
 $data = mysqli_fetch_assoc($result);

 if (!count($data)) {
 echo "<script>alert('Data tidak ditemukan pada
database');window.location='index.php';</script>";
 }
 } else {

 echo "<script>alert('Masukkan data id.');window.location='index.php';</script>";
 }
 ?>
<!DOCTYPE html>
<html>
 <head>
 <title>Data Mahasiswa</title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 button {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 border: 0px;
 margin-top: 20px;
 }
 label {
 margin-top: 10px;
 float: left;
 text-align: left;
 width: 100%;
 }
 input {
 padding: 6px;
 width: 100%;
 box-sizing: border-box;
 background: #f8f8f8;
 border: 2px solid #ccc;
 outline-color: salmon;
 }
 div {width: 100%;
 height: auto;
 }
 .base {
 width: 400px;
 height: auto;
 padding: 20px;
 margin-left: auto;
 margin-right: auto;
 background: #ededed;
 }
 </style>
 </head>
 <body>
 <center>
 <h1>Edit Mata Kuliah<?php echo $data['nama_matkul']; ?></h1>
 <center>
 <form method="POST" action="proses_editmk.php" enctype="multipart/form-data" >
 <section class="base">
 <!-- menampung nilai id produk yang akan di edit -->
 <input name="kode_mk" value="<?php echo $data['kode_mk']; ?>" hidden />
 <div>
 <label>Nama Mata Kuliah</label>
 <input type="text" name="nama_matkul" value="<?php echo
$data['nama_matkul']; ?>" autofocus="" required="" />
 </div>
 <div>
 <label>Sks</label>
 <input type="text" name="sks" value="<?php echo $data['sks']; ?>"
/>
 </div>
 <label>Semester</label>
 <input type="text" name="semester" value="<?php echo $data['semester']; ?>"
/>
 </div>
 
 
 <div>
 <button type="submit">Simpan Perubahan</button>
 </div>
 </section>
 </form>
 </body>
 </html>