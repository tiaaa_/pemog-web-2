<?php
 include('koneksi.php'); 

?>
<!DOCTYPE html>
<html>
 <head>
 <title> Data Mahasiswa </title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 table {
 border: solid 1px #DDEEEE;
 border-collapse: collapse;
 border-spacing: 0;
 width: 70%;
 margin: 10px auto 10px auto;
 }
 table thead th {
 background-color: #DDEFEF;
 border: solid 1px #DDEEEE;
 color: #336B6B;
 padding: 10px;
 text-align: left;
 text-shadow: 1px 1px 1px #fff;
 text-decoration: none;
 }
 table tbody td {
 border: solid 1px #DDEEEE;
 color: #333;
 padding: 10px;
 text-shadow: 1px 1px 1px #fff;
 }
 a {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 }
 </style>
 </head>


 <body>
 <center><h1>Data Mahasiswa</h1><center>
 <center><a href="tambah_mhs.php">+ &nbsp; Tambah Data</a><center>


 <br/>
 <table>
 <thead>
 <tr>
<th>No</th>
 <th>Nim</th>
 <th>Nama</th>
 <th>Alamat</th>
 <th>Jurusan</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>

 <?php

 $query = "SELECT * FROM tb_mhs ORDER BY nim ASC";
 $result = mysqli_query($koneksi, $query);
 if(!$result){
    die ("Query Error: ".mysqli_errno($koneksi).
    " - ".mysqli_error($koneksi));
    }
    
    $no = 1; 
    while($row = mysqli_fetch_assoc($result))
    {
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo $row['nim']; ?></td>
    <td><?php echo $row['nama']; ?></td>
    <td><?php echo $row['alamat']; ?></td>
    <td><?php echo $row['prodi']; ?></td>
    <td> <a href="edit_mhs.php?nim=<?php echo $row['nim']; ?>">Edit</a> |
    <a href="proses_hapusmhs.php?nim=<?php echo $row['nim']; ?>" onclick="return
   confirm('Anda yakin akan menghapus data ini?')">Hapus</a></td>
   
    </tr>
   
    <?php
    $no++; //untuk nomor urut terus bertambah 1
    }
    ?>

    
    </tbody>
    </table>

<center><h1>Mata Kuliah</h1><center>
<center><a href="tambah_mk.php">+ &nbsp; Tambah Mata Kuliah</a><center>
</br>
 <table>
 <thead>
 <tr>
<th>No</th>
 <th>Kode Mk</th>
 <th>Mata Kuliah</th>
 <th>Sks</th>
 <th>Semester</th>
 <th>Action</th>
 </tr>
 </thead>
 <tbody>

 <?php
 $query = "SELECT * FROM tb_mk ORDER BY kode_mk ASC";
 $result = mysqli_query($koneksi, $query);
 if(!$result){
    die ("Query Error: ".mysqli_errno($koneksi).
    " - ".mysqli_error($koneksi));
    }
    
    $no = 1; 
    while($row = mysqli_fetch_assoc($result))
    {
    ?>
    <tr>
    <td><?php echo $no; ?></td>
    <td><?php echo $row['kode_mk']; ?></td>
    <td><?php echo $row['nama_matkul']; ?></td>
    <td><?php echo $row['sks']; ?></td>
    <td><?php echo $row['semester']; ?></td>
    <td> <a href="edit_mk.php?kode_mk=<?php echo $row['kode_mk']; ?>">Edit</a> |
    <a href="proses_hapusmk.php?kode_mk=<?php echo $row['kode_mk']; ?>" onclick="return
   confirm('Anda yakin akan menghapus data ini?')">Hapus</a></td>
   
    </tr>
   
    <?php
    $no++; //untuk nomor urut terus bertambah 1
    }
    ?>
    </body>
   </html>
   
