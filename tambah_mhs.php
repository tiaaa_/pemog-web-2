<?php
 include('koneksi.php'); 

?>
<!DOCTYPE html>
<html>
 <head>
 <title>DATA MAHASISWA</title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 button {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 border: 0px;
 margin-top: 20px;
 }
 label {
 margin-top: 10px;
 float: left;
 text-align: left;
 width: 100%;
 }
 input {
 padding: 6px;
 width: 100%;
 box-sizing: border-box;
 background: #f8f8f8;
 border: 2px solid #ccc;
 outline-color: salmon;
 }
 div {
 width: 100%;
 height: auto;
 }
 .base {
 width: 400px;
 height: auto;
 padding: 20px;
 margin-left: auto;
 margin-right: auto;
 background: #ededed;
 }
 </style>
 </head>
 <body>
 <center>
 <h1>Tambah Data</h1>
 <center>
 <form method="POST" action="proses_tambahmhs.php" enctype="multipart/form-data" >
 <section class="base">
 <div>
 <label>Nim</label>
 <input type="text" name="nim" autofocus="" required="" />
 </div>
 <div>
 <label>Nama</label>
 <input type="text" name="nama" />
 </div>
 <label>Alamat</label>
 <input type="text" name="alamat" />
 </div>
 <label>Prodi</label>
 <input type="text" name="prodi" />
 </div>
 <button type="submit">Simpan Data</button>
 </div>
 </section>
 </form>
 </body>
</html>