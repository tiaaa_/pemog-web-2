<?php
 include('koneksi.php'); 

?>
<!DOCTYPE html>
<html>
 <head>
 <title>Jadwal Mata Kuliah</title>
 <style type="text/css">
 * {
 font-family: "Trebuchet MS";
 }
 h1 {
 text-transform: uppercase;
 color: salmon;
 }
 button {
 background-color: salmon;
 color: #fff;
 padding: 10px;
 text-decoration: none;
 font-size: 12px;
 border: 0px;
 margin-top: 20px;
 }
 label {
 margin-top: 10px;
 float: left;
 text-align: left;
 width: 100%;
 }
 input {
 padding: 6px;
 width: 100%;
 box-sizing: border-box;
 background: #f8f8f8;
 border: 2px solid #ccc;
 outline-color: salmon;
 }
 div {
 width: 100%;
 height: auto;
 }
 .base {
 width: 400px;
 height: auto;
 padding: 20px;
 margin-left: auto;
 margin-right: auto;
 background: #ededed;
 }
 </style>
 </head>
 <body>
 <center>
 <h1>Tambah Mata Kuliah</h1>
 <center>
 <form method="POST" action="proses_tambahmk.php" enctype="multipart/form-data" >
 <section class="base">
 <div>
 <label>Kode MK</label>
 <input type="text" name="kode_mk" autofocus="" required="" />
 </div>
 <div>
 <label>Mata Kuliah</label>
 <input type="text" name="nama_matkul" />
 </div>
 <label>Sks</label>
 <input type="text" name="sks" />
 </div>
 <label>Semester</label>
 <input type="text" name="semester" />
 </div>
 <button type="submit">Simpan Mata Kuliah</button>
 </div>
 </section>
 </form>
 </body>
</html>